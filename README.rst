MPI Ping-pong Benchmark Example for the GT Jinx cluster
=======================================================

* This page: https://bitbucket.org/rvuduc/mpi-pingpong/overview
* Info on the Jinx cluster: http://support.cc.gatech.edu/facilities/instructional-labs/jinx-cluster

In this hands-on lab, you will estimate the latency and bandwidth of message passing on the Jinx cluster, using the asynchronous ("immediate") send/receive primitives in MPI.

For an MPI tutorial or refresher, refer to Prof. Vuduc's guest lecture materials from the Spring 2014 offering of CSE 6220: http://hpcgarage.org/cse6220sp14


Getting Started
---------------

If you haven't done so already, you'll need to set up your environment on the Jinx cluster to do this exercise. If you've already done this step, you can skip to the next section, "Get the Example Code."

Start by logging into Jinx and setting up your environment so that the code we are providing will compile and run. To log in, you'll need an ``ssh`` (Secure Shell) client. In a typical *nix environment or from a Mac OS X terminal, you'd do this using,

.. code-block:: bash

  ssh GT_LOGIN@jinx-login.cc.gatech.edu


substituting your GT login for ``GT_LOGIN``.

This lab's experiments rely on a particular version of *GCC* as well as an *MPI* communication library discussed in class. Recall that MPI provides a portable interface for implementing distributed memory parallel programs.

Set up your shell environment for compiling and running MPI programs. Add the following lines to your ``.bashrc`` file in your home directory. (Go ahead and create this file if it does not already exist.)

.. code-block:: bash

  # Add these lines to ~/.bashrc, if not there already:
  if [ -f /etc/bashrc ]; then
    source /etc/bashrc
  fi

  # Add this line for MPI (+ gcc 4.7.2) support
  source /nethome/rvuduc3/local/jinx/setup-mpi.sh


You may also need to add the following line to ``.bash_profile``, also in your home directory. (Again, create this file if it does not already exist.)

.. code-block:: bash

  # Add these lines to ~/.bash_profile, if not there already:
  if [ -f ~/.bashrc ] ; then
    source ~/.bashrc
  fi


Once you've made these changes, log out and log back in for this setup to take effect. If it worked, then the command

.. code-block:: bash

  which mpirun


should return the string, ``/nethome/rvuduc3/local/jinx/openmpi/1.6.2--gcc4.7.2/bin/mpirun``.

  *Note*: For future reference, this exercise assumes version 1.6.2 of `OpenMPI <http://www.open-mpi.org>`_, not to be confused with `OpenMP <http://openmp.org/wp>`_. The other major open-source MPI implementation is `MPICH <http://www.mcs.anl.gov/research/projects/mpich2>`_. Many of the major cluster, supercomputer, and networking vendors (e.g., Intel, IBM, Cray, Mellanox), provide their own implementations of MPI as well.


Measuring computation and communication overlap
=====================================

Recall that MPI has both *blocking* and *non-blocking* ("immediate" or "asynchronous") send and receive primitives. ``MPI_Send`` is an example of a blocking operation; ``MPI_Isend`` ("immediate send") is an example of a non-blocking one. Let's start by making sure we understand the essential difference.

The signature for the blocking `MPI_Send` is as follows.

.. code-block:: c

  MPI_Send (message_buffer, message_length, element_type,
            destination_rank, message_tag, communicator);


The MPI standard says that only *after* ``MPI_Send (message_buffer, ...)`` returns is it safe for the caller to modify ``message_buffer``.

The signature and intended calling sequence of the non-blocking form, ``MPI_Isend``, is as follows.

.. code-block:: c

  MPI_Request request_handle;
  MPI_Isend (message_buffer, message_length, element_type,
             destination_rank, message_tag, communicator,
             &request_handle);
  //
  // ... can do other work here ...
  //
  MPI_Wait (&request_handle);


The non-blocking form has an extra argument, ``&request_handle``, which may be used to check on the status of the operation. The standard says that it is **not safe** for the caller to modify ``message_buffer`` until ``MPI_Wait``, called on the same request handle, returns. The non-blocking form implies that the MPI implementation **may** choose to return immediately. If it does so, the caller has a way to overlap computation with communication.


Checking the degree of overlap
------------------------------

In this lab, you will work with an MPI microbenchmark that can measure how well the underlying MPI implementation allows overlap. This microbenchmark has a function, ``async_comm_test``, whose code we have reproduced below. Read this code fragment now and make sure you understand what it does.

.. code-block:: c

  /** Pauses for approximately the specified time, in seconds. */
  double busywait (const double t_delay) { /* ... some code ... */ }

  double
  async_comm_test (const double t_delay, const int rank, int* msgbuf, const int len)
  {
    const int MSG_TAG = 1000; /* Arbitrary message tag number */
    double t_start = MPI_Wtime ();
    if (rank == 0) {
      MPI_Request req;
      MPI_Status stat;
      MPI_Isend (msgbuf, len, MPI_INT, 1, MSG_TAG, MPI_COMM_WORLD, &req);
      busywait (t_delay);
      MPI_Wait (&req, &stat);
    } else { /* rank == 1 */
      MPI_Status stat;
      MPI_Recv (msgbuf, len, MPI_INT, 0, MSG_TAG, MPI_COMM_WORLD, &stat);
    }
    return MPI_Wtime () - t_start;
  }


  *Note*: The function ``MPI_Wtime`` is a timer that MPI provides, which returns an elapsed time (in seconds) since some starting point.

To make sure you understand it, how would you would answer the following questions?

**Question 1:** Suppose we set ``t_delay`` to 0 and observe the return value of this function on rank 0. What does the return value tell you?

**Question 2:** Suppose we now gradually increase ``t_delay``. Describe what you would expect to happen to the return value on rank 0 if computation and communication are not overlapped. What you would instead observe if they do overlap?


Getting the Example Code
------------------------

The code for this exercise is hosted in a public repository on Bitbucket.org. This easiest way to get the code is to use `git <http://git-scm.org>`_, which is available on Jinx. To get it:

.. code-block:: bash

  # Check out the code into a new directory, 'mpi-pingpong'
  git clone https://bitbucket.org/rvuduc/mpi-pingpong.git

  # Change to the new directory
  cd mpi-pingpong

  # List the files
  ls


The file, ``async.c``, implements the microbenchmark. Here's what it does:

* It creates a message of size 8 MiB, i.e., 8 "`mebibytes <http://en.wikipedia.org/wiki/Mebibyte>`_" or :math:`2^{23}` = 8388608 bytes.
* For various values of ``t_delay``, starting at ``t_delay=0``, it runs ``async_comm_test``.
* For each value of ``t_delay``, it prints ``t_delay`` and the elapsed time returned by ``async_comm_test`` as observed on rank 0. (The code ignores the return value of ``async_comm_test`` from rank 1.)


Compile the microbenchmark
--------------------------

To compile the microbenchmark, use ``mpicc``, which in our case is a MPI-aware wrapper script around the GNU compiler, ``gcc``. As such, it accepts the usual compiler command-line flags for optimization, preprocessing, and linking. Try running ``mpicc --version`` to confirm this fact.

To compile our MPI program, ``async.c``, into a binary executable, ``async``, we would invoke the compiler as follows:

.. code-block:: c

  mpicc -o async async.c


*Note*: Since ``mpicc`` is a wrapper around the underlying compiler, you may wish to specify additional compiler options, like the optimization level or the flags to embed debugging symbols.


Running the microbenchmark
--------------------------

Let's run the microbenchmark between two nodes of the cluster, with one MPI rank on each. To do so, we will need to use a batch script.

  *Note*: Recall that in class we briefly mentioned that you can run multiple MPI ranks on the same node. However, our goal in this lab is to assess communication between separate nodes, rather than within a single node.

A *batch job script* is just a shell script containing two parts: (i) the commands needed to run your program; and (ii) metadata describing your job's resource needs, which appear in the script as comments at the top of the script. We give an example below for the ``async`` benchmark; this script is ``async.pbs``, which we have provided in the repo.

The ``async.pbs`` script asks the scheduler for (a) two nodes, (b) a maximum of 1 minute of wallclock time to execute, and (c) includes the MPI command-line program needed to launch ``async``. This metadata is embedded as ``#PBS`` comments, which the central system scheduler parses in order to determine where and when to run your job.

.. code-block:: bash
  #PBS -q class
  #PBS -l nodes=2
  #PBS -l walltime=00:01:00
  #PBS -N async

  export OMPI_MCA_mpi_yield_when_idle=0
  cd $PBS_O_WORKDIR

  echo "*** STARTED: `date` on `hostname` ***"
  echo $PWD
  cat $PBS_NODEFILE
  echo -e "\n\n"

  # Run the program
  mpirun --hostfile $PBS_NODEFILE -np 2 ./async

  echo "*** COMPLETED: `date` on `hostname` ***"

  # eof


  *Note*: Given an MPI executable, the ``mpirun`` command launches the program with the number of parallel processes (tasks) given by the ``-np`` argument. For the ``async`` microbenchmark, we need two such tasks as shown here. Since the job script metadata asks for two nodes, the MPI tasks or ranks will be allocated one per node.

To submit this job request, use the ``qsub`` command:

.. code-block:: bash

  qsub async.pbs


This command will register your job request and return a job identification number (job ID). Your job is first entered into a central queue, and depending on the current cluster load, might not run right away. As such, you can check on the status of your job requests by running:

.. code-block:: bash

  qstat -a


  *Note*: On Jinx with this MPI, you *may* see the error below in the ".e*" file produced after your job finishes:

.. code-block:: bash

  [jinx6][[17185,1],0][btl_openib_component.c:1673:init_one_device]
    error obtaining device context for mlx4_0 errno says No such file or directory
  --------------------------------------------------------------------------
  WARNING: There was an error initializing an OpenFabrics device.

    Local host:   jinx6
    Local device: mlx4_0
  --------------------------------------------------------------------------
  [jinx7][[17185,1],1][btl_openib_component.c:1673:init_one_device]
    error obtaining device context for mlx4_0 errno says No such file or directory


If you see this error, then something is wrong with one of the networking-related drivers on that node. You should report this to your peers on Piazza as well as to `helpdesk@cc.gatech.edu`, cc'ing your course instructors, including the TAs.


Analyzing the results
---------------------

If all went well, when the job completes you should see a new output file in the current directory: ``async.dat``. This file contains the raw benchmark results; inspect this now. The first column of this tab-delimited file is the delay parameter, ``t_delay``, in seconds; the second column is the return value from calling ``async_comm_test`` on rank 0, also in seconds.

  *Note*: We have also provided a simple *gnuplot* script for generating a plot of this raw data in PNG format. To generate it, run ``gnuplot async.gp``. (If you see an error related to "arial" being missing, you can ignore that.) If successful, it will create a file, ``async.png``. You can download this to your computer to look at it. Alternatively, if you logged into Jinx with X11 forwarding enabled, you may be able to view it directly using, for instance, the ``display`` command-line utility on Jinx.

**Question 3:** Based on your answer to Question 2 and these measured data, is computation-communication overlap occurring or not?

**Question 4:** Assume that the time to send a message of size :math:`n` has the form, :math:`T_{\mbox{msg}}(n) = \alpha + \frac{n}{\beta}`. We refer to :math:`\alpha` as the *latency* of messaging and :math:`\beta` as the *bandwidth*. Use the data you've collected to estimate these parameters of this hypothesized communication model.
